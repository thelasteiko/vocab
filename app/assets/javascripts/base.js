const GLOBAL = new class {
    constructor () {
        this.domain = "mlcs";
        this.protocol = "https";
        this.debug = true;
        this.level = 1;
        this.start_engine_scope = "missions";
    }
};
function log(str) {
  // console.log("This works");
  if (GLOBAL.debug) {
    // console.log("Global debug is on.");
    if (GLOBAL.level === 5) {
      if (!(log.caller === 'undefined'))
          console.log(log.caller+"::"+str);
      else
          console.log(str);
    } else {
      console.log(str);
    }
  }
}

function scope() {
  var sc = window.location.pathname;
  var i = sc.indexOf("/",1);
  if (i >= 0) {
    sc = sc.substring(1, i);
  }
  return sc;
}
function $(x) {
  // log(x);
  var that = document.querySelector(x);
  if (typeof that === 'undefined' || !that) {
    // log("Looking for id.");
    that = document.querySelector("#"+x);
  }
  if (typeof that === 'undefined' || !that) {
    // log("Looking for class.");
    that = document.querySelector("."+x);
  }
  return that;
}
function $$(x) {   // log(x);
  var that = document.querySelectorAll(x);
  if (typeof that === 'undefined' || !that) {
    // log("Looking for id.");
    that = document.querySelectorAll("#"+x);
  }
  if (typeof that === 'undefined' || !that) {
    // log("Looking for class.");
    that = document.querySelectorAll("."+x);
  }
  return that;
}
function ticker(str) {
  var t = $("#ticker");
  if (!(typeof t === "undefined")) {
    t.innerHTML = str;
  }
}
function create(str) {return document.createElement(str);}
//removes HTML tag characters
function safer(str) {
    //console.log("Safing " + str);
    return str.replace(/[<>\/"]/g,"");
}
Element.prototype.on = function (event_name, response) {
  this.addEventListener(event_name,response);
}
document.on = function (selector, event_name, response) {
  Array.from(this.querySelectorAll(selector)).forEach(el => {
    el.addEventListener(event_name, response);
  });
}
Element.prototype.insert = function (type,str="") {
    var i = type;
    if (type === "text") {
        i = document.createTextNode(safer(str));
    } else if (typeof type === "string") {
        if (str.length > 0){
            i = create(type).insert("text",str);
        } else {
            i = create(type);
        }
    }
    this.appendChild(i);
    return this;
}
Element.prototype.getFor = function() {
  return this.getAttribute("for");
}
Element.prototype.removeEvery = function(obj) {
  if (typeof obj == "string") {
    var that = this.querySelectorAll(obj);
    if (that) {
      for (t in that) {
        this.removeChild(that[t]);
      }
    }
  }
}
Element.prototype.removeAll = function() {
  while (this.firstChild) {
    this.removeChild(this.firstChild);
  }
}
Element.prototype.doclick = function() {
  var event = new MouseEvent('click', {
    view: window,
    bubbles: true,
    cancelable: true
  });
  return this.dispatchEvent(event);
}
