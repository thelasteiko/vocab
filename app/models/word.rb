class Word < ApplicationRecord
  validates :definition, presence: true, length: {
    minimum: 2,
    message: "Definition cannot be empty"
  }
  validates :part_of_speech, inclusion: {
    in: %w(verb noun adjective),
    message: "%{value} must be verb, noun or adjective"
  }
  validates :score, numericality: {only_integer: true,
    greater_than_or_equal_to: 0,
    less_than_or_equal_to: 100
  }
  def definition_list
    return definition.split(";")
  end
  # Retrieve words that are in the definition of this word
  # and in the list of words....ugh
  # def related_words
  #   rwords = Word.
  # end
  def increment_score
    self.score += 1 if self.score < 100
  end
  def decrement_score
    self.score -= 1 if self.score > 0
  end
  # get a random word from the highest scores
  def self.retrieve(l=1)
    i = 5
    w2 = Word.maximum(:score)
    w1 = w2-i
    r = Word.where(score: w1..w2).limit(l).order("RANDOM()")
    while r.length < l
      w1 = w2-i # increase range
      r = Word.where(score: w1..w2).limit(l).order("RANDOM()")
      i += 1
    end
    return r
  end
end
