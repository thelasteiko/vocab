class FlashCardsController < ApplicationController
  def index
    @word = Word.retrieve()[0]
  end
  def show
    @word = Word.find(params[:id])
    respond_to do |format|
      format.js
    end
  end
  def edit
    @word = Word.find(params[:id])
    # logger.debug(params)
    if params[:score_action] == "increment"
      @word.increment_score
      @word.save
    elsif params[:score_action] == "decrement"
      @word.decrement_score
      @word.save
    end
    redirect_to "/games/flash_cards"
  end
end
