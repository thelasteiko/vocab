class WordsController < ApplicationController
  def new
    @word = Word.new
  end
  def edit
    @word = Word.find(params[:id])
  end
  def update
    @word = Word.find(params[:id])
    if @word.update(word_params)
      redirect_to @word
    else
      render 'edit'
    end
  end
  def create
    # render plain: params[:word].inspect
    @word = Word.new(word_params)
    if @word.save
      redirect_to @word
    else
      render 'new'
    end
  end
  def show
    @word = Word.find(params[:id])
    @definitions = @word.definition_list
  end
  def index
    @words = Word.all.page(params[:page] || 1)
  end
  def destroy
    @word = Word.find(params[:id])
    @word.destroy
    redirect_to words_path
  end

  private
  def word_params
    h = params.require(:word).permit(:word, :part_of_speech, :definition)
    h[:editable] = true
    return h
  end
  # def definition_list
  #   return [] if not @word
  #   return @word.definition.split(';')
  # end
end
