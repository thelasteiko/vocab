class CreateWords < ActiveRecord::Migration[5.1]
  def change
    create_table :words do |t|
      t.string  :word, null: false
      t.string  :part_of_speech, null: false, default: ""
      t.text  :definition
      t.text  :sentence, default: ""
      t.integer :score, default: 50

      t.boolean :editable, default: false

      t.timestamps null: false
    end
    add_index :words, :word
  end
end
