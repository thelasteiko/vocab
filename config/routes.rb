Rails.application.routes.draw do
  get 'welcome/index'
  get 'games' => 'games#index'
  get 'games/flash_cards' => 'flash_cards#index'
  get 'games/flash_cards/:id' => 'flash_cards#show'
  post 'games/flash_cards/:id/edit' => 'flash_cards#edit'
  # get 'games/:game' => 'games#show'
  get "/statics/:static" => 'statics#show'

  resources :words

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "welcome#index"

end
